#ifndef GIT_COMPAT_MSVC_H
#define GIT_COMPAT_MSVC_H

#if !defined(_MSC_VER) || (_MSC_VER < 1400)
#  error You need at least Visual Studio 2005
#endif

#define inline		__inline
#define va_copy(d, s)	do { (d) = (s); } while(0)

#define SHA1_HEADER "mozilla-sha1/sha1.h"

#define NO_OPENSSL
#define NO_ICONV

#define NO_MKDTEMP /* support for this may be added later */
#define NO_STRCASESTR
#define NO_STRLCPY
#define NO_STRTOUMAX
#define NO_MEMMEM
#define INTERNAL_QSORT /* use internal qsort to benefit from ltcg */

#include <gitversion.h>

#define GIT_EXEC_PATH			msvc_get_exec_path()
#define ETC_GITCONFIG			msvc_get_etc_gitconfig()	
#define GIT_MAN_PATH			msvc_get_man_path()
#define GIT_INFO_PATH			msvc_get_info_path()
#define GIT_HTML_PATH			msvc_get_html_path()
#define DEFAULT_GIT_TEMPLATE_DIR	msvc_get_template_path()
#define GIT_USER_AGENT			"git/" GIT_VERSION

const char *msvc_get_exec_path();
const char *msvc_get_etc_gitconfig();	/* GIT_EXEC_PATH "/etc/gitconfig" */
const char *msvc_get_man_path();	/* GIT_EXEC_PATH "/man" */
const char *msvc_get_info_path();	/* GIT_EXEC_PATH "/info" */
const char *msvc_get_html_path();	/* GIT_EXEC_PATH "/html" */
const char *msvc_get_template_path();	/* GIT_EXEC_PATH "/template" */

#define has_dos_drive_prefix(path)	(isalpha(path[0]) && path[1] == ':')
#define is_dir_sep(c)			(c == '/' || c == '\\')
#define PATH_SEP			';'

#endif /* GIT_COMPAT_MSVC_H */
