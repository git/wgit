#include "../git-compat-util.h"
#include <windows.h>

inline void utf16_to_utf8(char *dst, const wchar_t *src, size_t len)
{
	if (!WideCharToMultiByte(CP_UTF8, 0, src, len, dst, len, NULL, NULL))
		die("failed to convert from UTF-16 to UTF-8: %d", GetLastError());
}

const char *msvc_get_exec_path()
{
	static char *exe_path;

	if (!exe_path) {
		wchar_t *wpath = NULL;
		size_t wlen = MAX_PATH / 4;
		char *path;
		size_t len;

		do {
			wlen *= 4;
			wpath = xrealloc(wpath, wlen * sizeof(wchar_t));
			len = GetModuleFileNameW(NULL, wpath, wlen);

		} while (len == wlen);

		path = xmalloc((len + 1) * sizeof(wchar_t));
		utf16_to_utf8(path, wpath, len + 1);
		free(wpath);

		len = (strrchr(path, '\\') - path);
		assert(len != ((char*) NULL - path));
		path[len] = '\0';
		exe_path = xrealloc(path, len + 1);
	}

	return exe_path;
}

const char *msvc_get_etc_gitconfig()
{
	static char *etc_path;

	if (!etc_path) {
		static const char etc[] = "/etc/gitconfig";
		const char *epath = msvc_get_exec_path();
		char *path;
		size_t len = strlen(epath);

		path = xmalloc(len + sizeof(etc));
		memcpy(path, epath, len);
		memcpy(path + len, etc, sizeof(etc));
		etc_path = path;
	}

	return etc_path;
}

const char *msvc_get_man_path()
{
	static char *man_path;

	if (!man_path) {
		static const char man[] = "/man";
		const char *epath = msvc_get_exec_path();
		char *path;
		size_t len = strlen(epath);

		path = xmalloc(len + sizeof(man));
		memcpy(path, epath, len);
		memcpy(path + len, man, sizeof(man));
		man_path = path;
	}

	return man_path;
}

const char *msvc_get_info_path()
{
	static char *info_path;

	if (!info_path) {
		static const char info[] = "/info";
		const char *epath = msvc_get_exec_path();
		char *path;
		size_t len = strlen(epath);

		path = xmalloc(len + sizeof(info));
		memcpy(path, epath, len);
		memcpy(path + len, info, sizeof(info));
		info_path = path;
	}

	return info_path;
}

const char *msvc_get_html_path()
{
	static char *html_path;

	if (!html_path) {
		static const char html[] = "/html";
		const char *epath = msvc_get_exec_path();
		char *path;
		size_t len = strlen(epath);

		path = xmalloc(len + sizeof(html));
		memcpy(path, epath, len);
		memcpy(path + len, html, sizeof(html));
		html_path = path;
	}

	return html_path;
}

const char *msvc_get_template_path()
{
	static char *template_path;

	if (!template_path) {
		static const char template[] = "/template";
		const char *epath = msvc_get_exec_path();
		char *path;
		size_t len = strlen(epath);

		path = xmalloc(len + sizeof(template));
		memcpy(path, epath, len);
		memcpy(path + len, template, sizeof(template));
		template_path = path;
	}

	return template_path;
}
